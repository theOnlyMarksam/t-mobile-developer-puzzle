# Code review
## Code smells
```
  this.store.select(getAllBooks).subscribe(books => {
     this.books = books;
   });
  ```
* subscription to observable is not being unsubscribed.
  Could also use `async` pipe in template
```
<img src="{{ b.coverUrl }}" />
```
* instead of string interpolation could use attribute binding
```
<strong>Published:</strong> {{ formatDate(b.publishedDate) }}
```
* angular has `date` pipe
```
<div class="book" data-testing="book-item" *ngFor="let b of books">
```
* `b` is not good variable name

## Improvements I would make
* make design responsive, currently there are always two columns for search result, 
which makes it difficult for people on narrow screens to browse search results
* effects use `HttpClient` directly, I would instead put these calls in service classes
and use services in effects
* reading list and book search should be placed in separate feature modules, because
they are two different features of the application
* currently there are no dependency constraints enforced

# Accessibility issues in the app
## Lighthouse extension finds
* *"Try searching for a topic, for example "JavaScript"."* text on initial page does not have enough contrast with background
* search button has no accessible name

## Manual check
* book cover image has no alt attribute
